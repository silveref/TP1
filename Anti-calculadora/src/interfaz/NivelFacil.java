package interfaz;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;

public class NivelFacil extends Nivel {

	public NivelFacil(JFrame frame) {
		super(frame);
		resultado.setBounds(370, 237, 46, 30);
		btnIgual.setBounds(315, 237, 46, 30);
		txtFactor1.setBounds(150, 237, 46, 30);
		btnSigno1.setBounds(205, 237, 46, 30);
		txtFactor2.setBounds(260, 237, 46, 30);
		
		mostrarNumYSig(2, 1);
		
//		Acción del botón "Igual" que utiliza el método verificar().
		btnIgual.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					int factor1 = Integer.valueOf(txtFactor1.getText());
					int factor2 = Integer.valueOf(txtFactor2.getText());

					listNumUsuario.clear();
					listNumUsuario.add(factor1);
					listNumUsuario.add(factor2);
					
					String signo = btnSigno1.getText();
					listOperUsuario.clear();
					listOperUsuario.add(signo.charAt(0));

					if (verificar())
						acertado();
					else
						equivocado();
				} catch (Exception exc) {
				}
				
			}
		});
	}

	public void reiniciar() {
		super.reiniciar();
		mostrarNumYSig(2, 1);
	}

}
