package interfaz;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;

import negocio.Generador;

public class NivelNormal extends Nivel {
	private JTextField textoFactor3;
	private JButton botonSigno2;

	public NivelNormal(JFrame frame) {
		super(frame);
		resultado.setBounds(435, 237, 46, 30);
		btnIgual.setBounds(380, 237, 46, 30);
		txtFactor1.setBounds(105, 237, 46, 30);
		btnSigno1.setBounds(160, 237, 46, 30);
		txtFactor2.setBounds(215, 237, 46, 30);
		
		botonSigno2 = new JButton("+");
		botonSigno2.setFont(btnFuente);	
		botonSigno2.setBounds(270, 237, 46, 30);
		panel.add(botonSigno2);
		
		textoFactor3 = new JTextField();
		textoFactor3.setFont(txtFuente);
		textoFactor3.setBounds(325, 237, 46, 30);
		textoFactor3.setColumns(10);
		soloNumeros(textoFactor3);
		panel.add(textoFactor3);
		
		numRandom = Generador.numeroRandom(3);
		numObjetivo.setText(String.valueOf(numRandom));
		
		mostrarNumYSig(3, 2);
		
//		Acción que cambia el signo
		botonSigno2.addActionListener(new ActionListener() {
			int indice = 0;
			public void actionPerformed(ActionEvent e) {
				indice = cambiarSigno(botonSigno2, indice);
			}
			
		});
		
//		Acción del botón "Igual" que utiliza el método verificar().
		btnIgual.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					int factor1 = Integer.valueOf(txtFactor1.getText());
					int factor2 = Integer.valueOf(txtFactor2.getText());
					int factor3 = Integer.valueOf(textoFactor3.getText());
					listNumUsuario.clear();
					listNumUsuario.add(factor1);
					listNumUsuario.add(factor2);
					listNumUsuario.add(factor3);
					
					String signo1 = btnSigno1.getText();
					String signo2 = botonSigno2.getText();
					listOperUsuario.clear();
					listOperUsuario.add(signo1.charAt(0));
					listOperUsuario.add(signo2.charAt(0));
					
					if (verificar())
						acertado();
					else
						equivocado();
				} catch (Exception exc) {
				}
				
			}
		});
	}
	
	public void reiniciar() {
		super.reiniciar();
		botonSigno2.setText("+");
		textoFactor3.setText("");
		mostrarNumYSig(3, 2);
		habilitar(true);
	}

	protected void habilitar(boolean estado) {
		super.habilitar(estado);
		botonSigno2.setEnabled(estado);
		textoFactor3.setEnabled(estado);
		
	}

}
