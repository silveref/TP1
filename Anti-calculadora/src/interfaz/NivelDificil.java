package interfaz;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;

import negocio.Generador;

public class NivelDificil extends Nivel {
	private JButton botonSigno2;
	private JTextField textoFactor3;
	private JButton botonSigno3;
	private JTextField textoFactor4;

	public NivelDificil(JFrame frame) {
		super(frame);
		resultado.setBounds(500, 237, 46, 30);
		btnIgual.setBounds(445, 237, 46, 30);
		txtFactor1.setBounds(60, 237, 46, 30);
		btnSigno1.setBounds(115, 237, 46, 30);
		txtFactor2.setBounds(170, 237, 46, 30);
		
		botonSigno2 = new JButton("+");
		botonSigno2.setFont(btnFuente);	
		botonSigno2.setBounds(225, 237, 46, 30);
		panel.add(botonSigno2);
		
		textoFactor3 = new JTextField();
		textoFactor3.setFont(txtFuente);
		textoFactor3.setBounds(280, 237, 46, 30);
		textoFactor3.setColumns(10);
		panel.add(textoFactor3);
		
		botonSigno3 = new JButton("+");
		botonSigno3.setFont(btnFuente);	
		botonSigno3.setBounds(335, 237, 46, 30);
		panel.add(botonSigno3);
		
		textoFactor4 = new JTextField();
		textoFactor4.setFont(txtFuente);
		textoFactor4.setBounds(390, 237, 46, 30);
		textoFactor4.setColumns(10);
		soloNumeros(textoFactor3);
		panel.add(textoFactor4);
		
		numRandom = Generador.numeroRandom(4);
		numObjetivo.setText(String.valueOf(numRandom));
		
		mostrarNumYSig(4, 3);
		
//		Acciones que cambian los signos		
		botonSigno2.addActionListener(new ActionListener() {
			int indice = 0;
			public void actionPerformed(ActionEvent e) {
				indice = cambiarSigno(botonSigno2, indice);
			}
		});		
		botonSigno3.addActionListener(new ActionListener() {
			int indice = 0;
			public void actionPerformed(ActionEvent e) {
				indice = cambiarSigno(botonSigno3, indice);
			}
		});	
		
//		Acción del botón "Igual" que utiliza el método verificar().		
		btnIgual.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					int factor1 = Integer.valueOf(txtFactor1.getText());
					int factor2 = Integer.valueOf(txtFactor2.getText());
					int factor3 = Integer.valueOf(textoFactor3.getText());
					int factor4 = Integer.valueOf(textoFactor4.getText());
					listNumUsuario.clear();
					listNumUsuario.add(factor1);
					listNumUsuario.add(factor2);
					listNumUsuario.add(factor3);
					listNumUsuario.add(factor4);
					
					String signo1 = btnSigno1.getText();
					String signo2 = botonSigno2.getText();
					String signo3 = botonSigno3.getText();
					listOperUsuario.clear();
					listOperUsuario.add(signo1.charAt(0));
					listOperUsuario.add(signo2.charAt(0));
					listOperUsuario.add(signo3.charAt(0));
					
					if (verificar())
						acertado();
					else
						equivocado();
				} catch (Exception exc) {
				}
				
			}
		});
	}

	public void reiniciar() {
		super.reiniciar();
		botonSigno2.setText("+");
		textoFactor3.setText("");
		botonSigno3.setText("+");
		textoFactor4.setText("");
		mostrarNumYSig(4, 3);
		habilitar(true);
	}
	
	protected void habilitar(boolean estado) {
		super.habilitar(estado);
		botonSigno2.setEnabled(estado);
		textoFactor3.setEnabled(estado);
		botonSigno3.setEnabled(estado);
		textoFactor4.setEnabled(estado);
	}
}
