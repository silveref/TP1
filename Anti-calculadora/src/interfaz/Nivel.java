package interfaz;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import negocio.Generador;
import negocio.Resolvente;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public abstract class Nivel extends Panel {
	
	protected JLabel lblOperadores;
	protected JLabel lblNumero;
	
	protected JLabel numObjetivo;
	protected JLabel operPermitidos;
	
	protected JButton btnVolver;
	protected JButton btnNuevo;

	protected JTextField txtFactor1;
	protected JTextField txtFactor2;
	protected JTextField resultado;
	protected JButton btnSigno1;
	protected JButton btnIgual;
	
	protected String[] listSignos = { "+", "-", "*", "/" };
//	El numero aleatorio que se mostrará en la interfaz.	
	protected int numRandom;
//	Recibe los operadores aleatorios.
	protected ArrayList<Character> listOperRandom;
//	Recibe los operadores usados por el usuario.
	protected ArrayList<Character> listOperUsuario = new ArrayList<Character>();
//	Recibe los números puestos por el usuario.
	protected ArrayList<Integer> listNumUsuario = new ArrayList<Integer>();

	public Nivel(JFrame frame) {
		super(frame);
		
		lblNumero = new JLabel("Número:");
		lblNumero.setFont(txtFuente);
		lblNumero.setBounds(180, 70, 150, 37);
		panel.add(lblNumero);
		
		lblOperadores = new JLabel("Operador/es:");
		lblOperadores.setFont(txtFuente);
		lblOperadores.setBounds(180, 130, 150, 37);
		panel.add(lblOperadores);
		
//		Botón que mostrará el panel de dificultades.		
		btnVolver = new JButton("Volver");
		btnVolver.setFont(btnFuente);
		btnVolver.setBounds(50, 300, 150, 37);
		panel.add(btnVolver);
		
//		Botón que reiniciará el nivel.
		btnNuevo = new JButton("Nuevo");
		btnNuevo.setFont(btnFuente);
		btnNuevo.setBounds(400, 300, 150, 37);
		panel.add(btnNuevo);
		
//		Número entero el cual que se debe obtener.		
		numObjetivo = new JLabel("");
		numObjetivo.setFont(txtFuente);
		numObjetivo.setBounds(380, 70, 46, 37);
		panel.add(numObjetivo);
		
//		Operadores permitidos.		
		operPermitidos = new JLabel("");
		operPermitidos.setFont(txtFuente);
		operPermitidos.setBounds(380, 130, 70, 37);
		panel.add(operPermitidos);
		
//		Mostrará el resultado de la cuenta hecha por el usuario.
		resultado = new JTextField("");
		resultado.setFont(txtFuente);
		resultado.setEditable(false);
		panel.add(resultado);
		
//		Realizará las operaciones matemáticas.		
		btnIgual = new JButton("=");
		btnIgual.setFont(btnFuente);
		panel.add(btnIgual);
		
		txtFactor1 = new JTextField();
		txtFactor1.setFont(txtFuente);
		txtFactor1.setColumns(10);
		soloNumeros(txtFactor1);
		panel.add(txtFactor1);
		
		btnSigno1 = new JButton("+");
		btnSigno1.setFont(btnFuente);	
		panel.add(btnSigno1);
		
		txtFactor2 = new JTextField();
		txtFactor2.setFont(txtFuente);
		txtFactor2.setColumns(10);
		soloNumeros(txtFactor2);
		panel.add(txtFactor2);
		
//		Reinicia el nivel.		
		btnNuevo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {	
				reiniciar();		
			}
		});

//		Acción que cambia el signo
		btnSigno1.addActionListener(new ActionListener() {
			int indice = 0;
			public void actionPerformed(ActionEvent e) {			
				indice = cambiarSigno(btnSigno1, indice);
			}
			
		});
	}

//	Asegura que el usuario solo utilice números.	
	protected void soloNumeros(JTextField factor) {
		factor.addKeyListener(new KeyAdapter() {
			public void keyTyped(KeyEvent e) {
				char caracter = e.getKeyChar();
				if (((caracter < '0') || (caracter > '9')) && (caracter != '\b' ))
					e.consume();
			}
		});
	}
	
//	Reinicia los componetes del nivel.	
	public void reiniciar() {
		txtFactor1.setText("");
		btnSigno1.setText("+");
		txtFactor2.setText("");
		resultado.setText("");
		resultado.setBackground(null);
		habilitar(true);
	}
	
//	Método que cambia el signo del botón dado.
	protected int cambiarSigno(JButton boton, int indice) {
		if (boton.getText().equals("+"))
			indice = 0;
		if (indice == 3)
			indice = 0;
		else
			indice++;
		boton.setText(listSignos[indice]);
		return indice;
	}
	
//	Muestra el número y los operadores aleatorios mediante los Generadores.
	protected void mostrarNumYSig(int numCifras, int cantSignos) {
		listOperRandom = Generador.signosRandom(cantSignos);
		String signos = "";
		for (Character signo : listOperRandom)
			signos += signo + " ";
		operPermitidos.setText(signos);
		
		numRandom = Generador.numeroRandom(numCifras);
		numObjetivo.setText(String.valueOf(numRandom));
	}
	
//	Verifica que el resultado de la cuenta del usuario sea igual al numero objetivo.	
	protected boolean verificar() {
//		Solo entra si el usuario usó los signos permitidos.		
		if (Resolvente.verificarSignos(listOperRandom, listOperUsuario)) {
			int resultUsuario = Resolvente.resolver(listOperUsuario, listNumUsuario);
			resultado.setText(String.valueOf(resultUsuario));
			int resultRandom = Integer.valueOf(numObjetivo.getText());
			return resultRandom == resultUsuario;
		}
		else {
			resultado.setText("");
			return false;
		}
		
	}
	
//	Indicio de que está mal.	
	protected void equivocado() {
		resultado.setBackground(Color.red);
	}

//	Indicio de que está bien.	
	protected void acertado() {
		resultado.setBackground(Color.green);
		habilitar(false);
	}

	protected void habilitar(boolean estado) {
		txtFactor1.setEnabled(estado);
		txtFactor2.setEnabled(estado);
		btnSigno1.setEnabled(estado);
		btnIgual.setEnabled(estado);
	}
	
//	Getter.	
	public JButton botonVolver() {
		return btnVolver;
	}	
}
