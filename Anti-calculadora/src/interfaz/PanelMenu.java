package interfaz;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class PanelMenu extends Panel {
	private JLabel titulo;
	private JLabel texto1;
	private JLabel texto2;
	private JButton empezar;

	public PanelMenu(JFrame frame) {
		super(frame);
//		Tíulo del menú		
		titulo = new JLabel("Anti-calculadora");
		titulo.setFont(titlFuente);
		titulo.setBounds(122, 50, 345, 49);
		panel.add(titulo);
		
//		Textos que explican el juego.
		texto1 = new JLabel("Obtener el número en pantalla a partir de los");
		texto1.setFont(txtFuente);
		texto1.setBounds(91, 147, 438, 37);
		panel.add(texto1);
		
		texto2 = new JLabel("operadores matemáticos permitidos.");
		texto2.setFont(txtFuente);
		texto2.setBounds(91, 179, 438, 37);
		panel.add(texto2);
		
//		El botón que va a mostrar los niveles de dificultad.		
		empezar = new JButton("Empezar");
		empezar.setFont(btnFuente);
		empezar.setBounds(230, 269, 135, 37);
		panel.add(empezar);
	}

//	Getter.	
	public JButton botonEmpezar() {
		return empezar;
	}
	
	

}
