package interfaz;

import java.awt.Font;

import javax.swing.JFrame;
import javax.swing.JPanel;

public abstract class Panel {
	protected JPanel panel;
	
//	Fuentes para la interfaz.
	protected Font titlFuente = new Font("Georgia", Font.BOLD, 38);
	protected Font txtFuente = new Font("Source Code Pro", Font.PLAIN, 15);
	protected Font btnFuente = new Font("Century Gothic", Font.BOLD, 18);
	
//	El constructor configura el panel (tamaño, etc.).	
	public Panel(JFrame frame) {
		panel = new JPanel();
		panel.setBounds(0, 0, 594, 372);
		frame.getContentPane().add(panel);
		panel.setLayout(null);
	}

//	Según el estado booleano, se mostrará o no el panel.
//	Con este método se jugará con los diferentes paneles para no mostrar más de uno a la vez.
	public void mostrar(boolean estado) {
		panel.setVisible(estado);
	}

}
