package interfaz;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class PanelDificultad extends Panel {
	
	private JButton btnFacil;
	private JButton btnNormal;
	private JButton btnDificil;
	
	private JLabel lblFacil;
	private JLabel lblNormal;
	private JLabel lblDificil;

	public PanelDificultad(JFrame frame) {
		super(frame);
//		Tíulo de la dificultad.
		JLabel titulo = new JLabel("Dificultad");
		titulo.setFont(titlFuente);
		titulo.setBounds(189, 50, 221, 49);
		panel.add(titulo);
		
//		Botones según los diferentes niveles.
		btnFacil = new JButton("Fácil");
		btnFacil.setFont(btnFuente);
		btnFacil.setBounds(77, 127, 135, 37);
		panel.add(btnFacil);
		
		btnNormal = new JButton("Normal");
		btnNormal.setFont(btnFuente);
		btnNormal.setBounds(77, 193, 135, 37);
		panel.add(btnNormal);
		
		btnDificil = new JButton("Difícil");
		btnDificil.setFont(btnFuente);
		btnDificil.setBounds(77, 259, 135, 37);
		panel.add(btnDificil);
		
//		Describen brevemente cada nivel.		
		lblFacil = new JLabel("números de 2 cifras y 1 operador.");
		lblFacil.setFont(txtFuente);
		lblFacil.setBounds(252, 130, 316, 37);
		panel.add(lblFacil);
		
		lblNormal = new JLabel("números de 3 cifras y 2 operadores.");
		lblNormal.setFont(txtFuente);
		lblNormal.setBounds(252, 196, 316, 37);
		panel.add(lblNormal);
		
		lblDificil = new JLabel("números de 4 cifras y 3 operadores.");
		lblDificil.setFont(txtFuente);
		lblDificil.setBounds(252, 262, 316, 37);
		panel.add(lblDificil);
	}

//	Getters.	
	public JButton botonFacil() {
		return btnFacil;
	}

	public JButton botonNormal() {
		return btnNormal;
	}

	public JButton botonDificil() {
		return btnDificil;
	}
	
}
