package interfaz;

import javax.swing.JFrame;
import javax.swing.UIManager;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Juego {
//	frame y paneles.
	private JFrame frame;
	private PanelMenu menu;
	private PanelDificultad niveles;
	private NivelFacil facil;
	private NivelNormal normal;
	private NivelDificil dificil;

	/**
	 * Create the application.
	 */
	public Juego() {
		try {
			//UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			UIManager.setLookAndFeel("com.jtattoo.plaf.graphite.GraphiteLookAndFeel");
		} catch(Exception e) {
			
		}
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
//		Configuración del frame.		
		frame = new JFrame();
		frame.setResizable(false);
		frame.setVisible(true);
		frame.setBounds(200, 100, 600, 400);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
//		Declaración de los paneles		
		menu = new PanelMenu(frame);
		niveles = new PanelDificultad(frame);
		niveles.mostrar(false);
		facil = new NivelFacil(frame);
		facil.mostrar(false);
		normal = new NivelNormal(frame);
		normal.mostrar(false);
		dificil = new NivelDificil(frame);
		dificil.mostrar(false);
		
//		Acción del botón "Empezar" del Menú		
		menu.botonEmpezar().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				menu.mostrar(false);
				niveles.mostrar(true);
			}
		});
		
//		Acciones de los botones de dificultad		
		niveles.botonFacil().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				niveles.mostrar(false);
				facil.mostrar(true);
				facil.reiniciar();
			}
		});	
		niveles.botonNormal().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				niveles.mostrar(false);
				normal.mostrar(true);
				normal.reiniciar();
			}
		});	
		niveles.botonDificil().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				niveles.mostrar(false);
				dificil.mostrar(true);
				dificil.reiniciar();
			}
		});
		
//		Acciones de los botones "Volver" de cada dificultad.
		facil.botonVolver().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				niveles.mostrar(true);
				facil.mostrar(false);
			}
		});	
		normal.botonVolver().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				niveles.mostrar(true);
				normal.mostrar(false);
			}
		});
		dificil.botonVolver().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				niveles.mostrar(true);
				dificil.mostrar(false);
			}
		});
		
	}	
	
}
