package negocio;

import java.util.ArrayList;
import java.util.Random;

public class Generador {

//	Devuelve un número aleatorio según la cantidad de cifras.	
	public static int numeroRandom(int digitos) {
		int numero;
		Random random = new Random();
		switch (digitos) {
		case 2: numero = random.nextInt(90)+10; break;
		case 3: numero = random.nextInt(900)+100; break;
		default: numero = random.nextInt(9000)+1000; break;	
		}
		return numero;
	}
	
//	Devuelve una lista de operadores aleatorios según la cantidad pedida.	
	public static ArrayList<Character> signosRandom(int cantidad) {
		ArrayList<Character> signos = new ArrayList<Character>();
		for (int i=0; i < cantidad; i++)
			signos.add(obtenerSigno());
		return signos;
	}

//	Devuelve el carácter del operador aleatoriamente.	
	private static Character obtenerSigno() {
		Random random = new Random();
		int indice = random.nextInt(4);
		Character signo;
		switch (indice) {
		case 0: signo = '+'; break;
		case 1: signo = '-'; break;
		case 2: signo = '*'; break;
		default: signo = '/'; break;
		}
		return signo;
	}
}