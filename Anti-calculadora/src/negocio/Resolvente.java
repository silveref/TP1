package negocio;

import java.util.ArrayList;

public class Resolvente {
	
//	Verifica que los operadores del usuario sean los permitidos.
	public static boolean verificarSignos(ArrayList<Character> sigRandom, ArrayList<Character> sigUsuario) {
		ArrayList<Character> aux = new ArrayList<Character>();
		for (Character signo : sigRandom)
			aux.add(signo);
		for (int i=0; i < sigUsuario.size(); i++) {
			int indice = 0;
			boolean cond = true;
			while (cond) {
				if (sigUsuario.get(i).equals(aux.get(indice))) {
					aux.remove(indice);
					cond = false;
				}	
				else if (indice == aux.size()-1)
					cond = false;
				else
					indice++;
			}
		}
		return aux.isEmpty();	
	}

//	Resuelve la cuenta propuesta por el Usuario.	
	public static int resolver(ArrayList<Character> sigUsuario, ArrayList<Integer> numUsuario) {
		int resultado = numUsuario.get(0);
		for (int i=0; i < sigUsuario.size(); i++) {
			char signo = sigUsuario.get(i);
			int factor2 = numUsuario.get(i+1);
			resultado = resolver(resultado, signo, factor2);
		}
		return resultado;
	}
//	Realiza la operación matemática según el signo.	
	private static int resolver(int num1, Character signo, int num2) {
		int resultado = 0;
		switch (signo) {
		case '+': resultado = num1 + num2; break;
		case '-': resultado = num1 - num2; break;
		case '*': resultado = num1 * num2; break;
		case '/': resultado = num1 / num2; break;
		}
		return resultado;
	}

}
